package org.fdroid.fdroid.nearby;

import android.annotation.TargetApi;
import android.content.Context;
import android.database.Cursor;
import android.util.AttributeSet;

import androidx.loader.app.LoaderManager;

/**
 * Placeholder to avoid modifying classes copied from fdroidclient.
 */
public abstract class SwapSuccessView extends SwapView implements LoaderManager.LoaderCallbacks<Cursor> {
    public SwapSuccessView(Context context) {
        super(context);
    }

    public SwapSuccessView(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    public SwapSuccessView(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
    }

    @TargetApi(21)
    public SwapSuccessView(Context context, AttributeSet attrs, int defStyleAttr, int defStyleRes) {
        super(context, attrs, defStyleAttr, defStyleRes);
    }
}
