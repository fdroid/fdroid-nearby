package org.fdroid.fdroid.data;

import android.content.Context;
import android.net.Uri;

import java.util.Collections;
import java.util.List;

/**
 * Placeholder to avoid modifying classes copied from fdroidclient.
 */
public class ApkProvider {

    public static class Helper {
        public static List<Apk> findByPackageName(Context context, String packageName) {
            return Collections.EMPTY_LIST;
        }

        public static Apk findApkFromAnyRepo(Context context, String packageName, int versionCode) {
            throw new IllegalStateException("unimplemented");
        }
    }

    public static Uri getContentUri() {
        return AppProvider.PLACEHOLDER_URI;
    }
}
