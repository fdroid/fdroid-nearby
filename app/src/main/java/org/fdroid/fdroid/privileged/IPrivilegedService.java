package org.fdroid.fdroid.privileged;

import android.content.pm.PackageInfo;
import android.os.IBinder;
import android.os.RemoteException;

import java.util.List;

/**
 * Placeholder to avoid modifying classes copied from fdroidclient.
 */
public interface IPrivilegedService {

    abstract class Stub {
        public static IPrivilegedService asInterface(IBinder service) {
            return null;
        }
    }

    boolean hasPrivilegedPermissions();

    List<PackageInfo> getInstalledPackages(int flags) throws RemoteException;
}
