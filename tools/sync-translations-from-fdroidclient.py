#!/usr/bin/env python3
#

import glob
import os
import re
from xml.etree import ElementTree


def get_strings_xml_keys(root):
    keys = set()
    for e in root.findall('.//string'):
        if e.text is None:
            continue
        keys.add(e.attrib['name'])
    return keys


fdroidclient_dir = os.path.join(os.path.dirname(__file__),'../../fdroidclient')
fdroid_nearby_dir = os.path.join(os.path.dirname(__file__), '..')

locale_pat = re.compile(r'.*values-([a-z][a-z][a-zA-Z-]*)/strings.xml')
translation_pat = re.compile(r'.*name="([a-zA-Z0-9_]+)"[^>]*>"?([^"<]*).*')

tree = ElementTree.parse(os.path.join(fdroid_nearby_dir, 'app/src/main/res/values/strings.xml'))
root = tree.getroot()
sourcekeys = get_strings_xml_keys(root)

for f in sorted(glob.glob(os.path.join(fdroidclient_dir, 'app/src/main/res/values-[a-z][a-z]*/strings.xml'))):
    m = locale_pat.search(f)
    if m:
        locale = m.group(1)
        if locale.endswith('nokeys') or locale.endswith('television') or locale.endswith('watch'):
            continue
    print(locale)
    with open(f) as fp:
        contents = fp.read()
    for m in translation_pat.finditer(contents):
        key = m.group(1)
        if key not in sourcekeys:
            continue
        word = m.group(2)
        fdroid = fdroid_nearby_dir + '/app/src/main/res/values-' + locale + '/strings.xml'
        print(fdroid)
        if not os.path.exists(fdroid):
            os.makedirs(os.path.dirname(fdroid), exist_ok=True)
            with open(fdroid, 'w') as fp:
                fp.write("""<?xml version="1.0" encoding="utf-8"?>\n<resources>\n</resources>""")
        print(locale, '\t', key, '\t', word)
        root = ElementTree.parse(fdroid).getroot()
        locale_keys = get_strings_xml_keys(root)
        with open(fdroid) as fp:
            data = fp.read()
        with open(fdroid, 'w') as fp:
            if key in locale_keys:
                fp.write(re.sub(r'"' + key + r'">[^<]+</string',
                                r'"' + key + r'">' + word + r'</string',
                                data))
            else:
                fp.write(re.sub(r'(\n\s*</resources>)',
                                r'\n    <string name="' + key + r'">' + word + r'</string>\1',
                                data))
