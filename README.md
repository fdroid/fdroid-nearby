F-Droid Nearby
--------------

F-Droid Nearby is a simple app for exchanging free software apps locally,
device-to-device, even when internet is not available or too expensive.  It is
compatible with the built-in Nearby feature of the F-Droid client app.

You can download it from F-Droid or Google Play:

[<img src="https://fdroid.gitlab.io/artwork/badge/get-it-on.png"
     alt="Get it on F-Droid"
     height="80">](https://f-droid.org/packages/org.fdroid.nearby/)
[<img src="https://play.google.com/intl/en_us/badges/images/generic/en-play-badge.png"
     alt="Get it on Google Play"
     height="80">](https://play.google.com/store/apps/details?id=org.fdroid.nearby)
